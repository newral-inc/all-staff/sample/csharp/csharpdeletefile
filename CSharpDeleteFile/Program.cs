﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CSharpDeleteFile
{
    class Program
    {
        static void Main(string[] args)
        {
            // 対象のファイルが存在しないことを確認します。
            var path = "sample.txt";
            Console.WriteLine($"File exists={File.Exists(path)}");

            // 対象のファイルを作成します。
            var fileStream = File.Create(path);
            fileStream.Close();
            Console.WriteLine($"File exists={File.Exists(path)}");

            // 対象のファイルを削除します。
            File.Delete(path);
            Console.WriteLine($"File exists={File.Exists(path)}");
        }
    }
}
